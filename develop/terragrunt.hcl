locals {
  env         = yamldecode(file(find_in_parent_folders(".env.yaml")))
  ENVIRONMENT = "develop"
}

# Indicate where to source the terraform module from.
# The URL used here is a shorthand for
# "tfr://registry.terraform.io/terraform-aws-modules/vpc/aws?version=3.5.0".
# Note the extra `/` after the protocol is required for the shorthand
# notation.
terraform {
  source = "git::git@gitlab.com:devops-open-project/terraform/terraform-proxmox-infrastructure.git?ref=master"
  //source = "../../terraform-proxmox-infrastructure"
}

# Indicate the input values to use for the variables of the module.
inputs = {
  environment = "${local.ENVIRONMENT}"
  vm_object = [
    {
      name            = "${local.ENVIRONMENT}-microk8s-0",
      clone           = "VM 9001",
      size            = "20G",
      cores           = 1,
      sockets         = 1,
      memory          = 4096,
      storage         = "local-lvm",
      target_node     = "${local.env.TARGET_NODE}",
      ipconfig        = "ip=192.168.0.116/24,gw=192.168.0.1",
      sshkeys         = "${local.env.SSH_KEYS}",
      user            = "${local.env.USER}"
    },
    {
      name            = "${local.ENVIRONMENT}-microk8s-1",
      clone           = "VM 9001",
      size            = "20G",
      cores           = 1,
      sockets         = 1,
      memory          = 4096,
      storage         = "local-lvm",
      target_node     = "${local.env.TARGET_NODE}",
      ipconfig        = "ip=192.168.0.117/24,gw=192.168.0.1",
      sshkeys         = "${local.env.SSH_KEYS}",
      user            = "${local.env.USER}"
    },
    {
      name            = "${local.ENVIRONMENT}-microk8s-2",
      clone           = "VM 9001",
      size            = "20G",
      cores           = 1,
      sockets         = 1,
      memory          = 4096,
      storage         = "local-lvm",
      target_node     = "${local.env.TARGET_NODE}",
      ipconfig        = "ip=192.168.0.118/24,gw=192.168.0.1",
      sshkeys         = "${local.env.SSH_KEYS}",
      user            = "${local.env.USER}"
    },
  ]
}

## TODO: Migrate to Global terragrunt
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite"
  contents  = <<EOF

terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.9.5"
    }
  }
}

provider "proxmox" {
  pm_tls_insecure = true
  pm_api_url      = "${local.env.PROXMOX_URL}"
  pm_parallel = 20
}

EOF
}

## TODO: Migrate to Global terragrunt
remote_state {
  backend = "http"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    address        = "${local.env.GITLAB_API_V4_URL}/projects/${local.env.GITLAB_PROJECT_ID}/terraform/state/${local.env.APP_NAME}-${local.ENVIRONMENT}"
    lock_address   = "${local.env.GITLAB_API_V4_URL}/projects/${local.env.GITLAB_PROJECT_ID}/terraform/state/${local.env.APP_NAME}-${local.ENVIRONMENT}/lock"
    unlock_address = "${local.env.GITLAB_API_V4_URL}/projects/${local.env.GITLAB_PROJECT_ID}/terraform/state/${local.env.APP_NAME}-${local.ENVIRONMENT}/lock"
    username       = "${local.env.GITLAB_USERNAME}"
    password       = "${local.env.GITLAB_TOKEN}"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}
